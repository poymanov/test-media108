// Кнопка добавления новых строк для стилей
var buttonAddStyle = document.querySelector('.addStyleField');

// Список со стилями
var styleList = document.querySelector('.form__list');

// Кнопка получения результатов
var buttonResults = document.querySelector('.getResults');

// Область результатов
var resultBlock = document.querySelector('.result');

// Функция, которая будет выполнена в результате верности условия
function actionFunction() {
	alert("Условие сработало. Выполняется функция.");
}

// Обработка нажатия кнопки добавления стилей
buttonAddStyle.addEventListener('click',function(e) {
	
	e.preventDefault();

	var itemStyle = document.createElement('li');
	itemStyle.classList.add('form__item');
	itemStyle.innerHTML = "<input type='text' data-style>";
	styleList.appendChild(itemStyle);

});

// Обработка получения результатов
buttonResults.addEventListener('click',function(e){

	e.preventDefault();

	// получаем параметры для передачи на сервер
	var paramA = document.getElementById('paramA');
	var paramB = document.getElementById('paramB');
	var formula = document.getElementById('formula');
	var condition = document.getElementById('condition');
	var conditionValue = document.getElementById('condition-value');

	// формируем строку с параметрами для отправки
	var params = "paramA="+paramA.value
				+"&paramB="+paramB.value
				+"&formula="+formula.value
				+"&condition="+condition.value
				+"&conditionValue="+conditionValue.value;				

	var xhr = new XMLHttpRequest();
	xhr.open('POST', '/server.php', true);
	xhr.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
	xhr.send(params);

	xhr.onreadystatechange = function() {
		if (xhr.readyState != 4) {return};

			// Очищаем блок результатов
			resultBlock.innerHTML = "";
			resultBlock.setAttribute("class","");
			resultBlock.setAttribute("style","");

			if (xhr.status = 200) {

				var responseObj = JSON.parse(xhr.responseText);
				var status = responseObj.status;
				var result = responseObj.result;


				if(status === 0) {
					// Если 0, то условие не сработало
					resultBlock.innerHTML = "Условие не сработало:<br>"+result;
				} else if(status === 1) {
					// В зависимости от обработчика выполняем действие

					// Получаем значение радиокнопок
					var processing = document.getElementsByName('processing');

					var processingValue = "";

					for (var i = 0, length = processing.length; i < length; i++) {
					    if (processing[i].checked) {
					        processingValue = processing[i].value;
					        break;
					    }
					}

					resultBlock.innerHTML = "Условие сработало:<br>"+result;

					// В зависимости от значения выполняем действия
					if (processingValue == "function") {
						// Вызываем функцию
						actionFunction();
					} else if (processingValue == "class") {
						// Устанавливаем класс
						resultBlock.classList.add('special-class');
					} else if (processingValue == "style") {

						// Получаем значения всех полей со стилями
						var styles = document.querySelectorAll('li [data-style]');
						var strStyles = "";

						// получаем все стили
						for(var i = 0; i < styles.length; i++) {
							strStyles = strStyles + styles[i].value+";";
						}

						// Добавляем в стили блока результата
						resultBlock.setAttribute('style',strStyles);

					}
				}
				

			}
	}
});


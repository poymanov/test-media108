<?php

// Получаем данные из ajax
$paramA = $_POST['paramA'];
$paramB = $_POST['paramB'];
$formula = $_POST['formula'];
$condition = $_POST['condition'];
$conditionValue = $_POST['conditionValue'];

// Все пробелы, получившиеся после передачи через ajax преобразуем в +
$formula = str_replace(" ", "+", $formula);

// Заменяем данные из строки формулы на значения переменных
$formula = str_replace("a", $paramA, $formula);
$formula = str_replace("b", $paramB, $formula);

// Рассчитываем формулу
$result = eval("return $formula;");

$returnResult = 0;
$stringResult = "";

// Сравниваем с заданным условием
// Возвращаем статус сравнения
// И строку с результами расчета
if($condition == "1") {
	if($result > $conditionValue) {
		$returnResult = 1;
	}
	$stringResult = $result." > ".$conditionValue;
} else if($condition == "2") {
	if($result < $conditionValue) {
		$returnResult = 1;		
	}
	$stringResult = $result." < ".$conditionValue;
} else if($condition == "3") {
	if($result == $conditionValue) {
		$returnResult = 1;
	}
	$stringResult = $result." == ".$conditionValue;	
} else if($condition == "4") {
	if($result != $conditionValue) {
		$returnResult = 1;
	}
	$stringResult = $result." != ".$conditionValue;		
}

// Возвращаем результат
$array = array();
$array['status'] = $returnResult;
$array['result'] = $stringResult;

header('Content-type: application/json');
echo json_encode($array);

?>

